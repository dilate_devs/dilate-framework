function hideParentTheme() {
    jQuery('.theme[aria-describedby="cardinal-action cardinal-name"]').remove();
}

jQuery(document).ready(function() {
    hideParentTheme();
});