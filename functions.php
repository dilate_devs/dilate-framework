<?php
	
	/*
	*
	*	Cardinal Functions - Child Theme
	*	------------------------------------------------
	*	These functions will override the parent theme
	*	functions. We have provided some examples below.
	*
	*
	*/

	/* LOAD & CACHE BUST PARENT THEME STYLES
	================================================== */
	function cardinal_child_enqueue_styles() {
		wp_enqueue_style( 'cardinal-parent-style', get_template_directory_uri() . '/style.css', false, filemtime( get_stylesheet_directory() . '/style.css' ), true);
	} add_action( 'wp_enqueue_scripts', 'cardinal_child_enqueue_styles' );
    
    /* UNLOAD CHILD THEME STYLE, RELOAD, CACHE BUST
	================================================== */
	function child_style() {
		wp_deregister_style( 'sf-main' );
		wp_dequeue_style( 'sf-main' );
		wp_register_style('sf-main-dilate', get_stylesheet_directory_uri() . '/style.css', array(), filemtime( get_stylesheet_directory().'/style.css' ), 'all');
		wp_enqueue_style('sf-main-dilate', get_stylesheet_directory_uri() . '/style.css', array(), filemtime( get_stylesheet_directory().'/style.css' ), 'all');
	} add_action( 'wp_enqueue_scripts', 'child_style', 999);

	/* LOAD & CACHE BUST ADMIN & FRONTEND SCRIPTS & STYLES
	================================================== */
	function enqueue_admin_styles() { // admin
		wp_enqueue_style( 'custom-admin', get_stylesheet_directory_uri() . '/admin.css', false, filemtime( get_stylesheet_directory() .'/admin.css'), true);
		wp_register_script('custom-admin_js', get_stylesheet_directory_uri() . '/admin.js', false, filemtime( get_stylesheet_directory().'/admin.js' ), true);
		wp_enqueue_script('custom-admin', get_stylesheet_directory_uri() . '/admin.js', false, filemtime( get_stylesheet_directory().'/admin.js' ), true);
	} add_action( 'admin_enqueue_scripts', 'enqueue_admin_styles' );

	function custom_script() { // frontend
		wp_register_script('theme_js', get_stylesheet_directory_uri() . '/script.js', false, filemtime( get_stylesheet_directory().'/script.js' ), true);
		wp_enqueue_script('theme_js', get_stylesheet_directory_uri() . '/script.js', false, filemtime( get_stylesheet_directory().'/script.js' ), true);
	} add_action( 'wp_enqueue_scripts', 'custom_script' );

	/* REMOVE NOTICES FOR NON-ADMINS
	================================================== */
	function remove_update_nags() {
		if (!current_user_can( 'administrator' )) {
			echo '<style>.update-nag, .updated, .error { display: none !important; }</style>';
		}
	} 	add_action('admin_enqueue_scripts', 'remove_update_nags');
		add_action('login_enqueue_scripts', 'remove_update_nags');

	/* LOAD CUSTOM ADMIN LOGO
	================================================== */
	function custom_login_logo() {
		echo '<style type="text/css">
					  .login div#login h1 a { background-image: url(' . get_stylesheet_directory_uri() .'/images/logo_admin.png) !important;
					  width: 100% !important; height: 95px !important; background-size: auto !important; }
				  </style>';

		echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>';

		echo '<script type="text/javascript">
					  jQuery(document).ready(function() {
						  jQuery(".login div#login h1 a").attr("href","http://www.dilate.com.au").attr("title","Maintained by Dilate Digital");
					  });
				  </script>';

	}
	add_action('login_head', 'custom_login_logo');

	/* REMOVE DEFAULT WORDPRESS POST, COMMENT & PLUGINS
	================================================== */
	function delete_annoying_stuff() {
		wp_delete_post(1, TRUE); // Delete Hello Worlda
		wp_delete_post(2, TRUE); // Delete Sample Page
		wp_delete_comment(1, TRUE); // Delete default comment
		if (file_exists(WP_PLUGIN_DIR.'/hello.php')) {
			require_once(ABSPATH.'wp-admin/includes/plugin.php');
			require_once(ABSPATH.'wp-admin/includes/file.php');
			delete_plugins(array('hello.php'));
		}
	} add_action( 'after_switch_theme', 'delete_annoying_stuff' );

	/* SET THE TIME TO GMT+8
	================================================== */
	function update_default_time() {
		update_option( 'gmt_offset', '+8' );
		update_option( 'timezone_string', '' );
	} add_action( 'after_switch_theme', 'update_default_time' );

	/* SET FRONT PAGE TO HOME (NOT FULLY WORKING)
	================================================== */
	function check_pages_live() {
		if(get_page_by_title( 'Home' ) == NULL) {
			create_pages('Home');
		}
	} add_action('after_switch_theme','check_pages_live');

	function create_pages($pageName) {
		$createPage = array(
			'post_title' => $pageName,
			'post_content' => 'Dilate - In the business of building businesses! Replace this text.',
			'post_status' => 'publish',
			'post_author' => 1,
			'post_type' => 'page',
			'post_name' => $pageName
		);

		wp_insert_post( $createPage );
	}

	function set_front_page() {
		$front = get_page_by_title('home');
		update_option('page_on_front', $front->ID);
		update_option('show_on_front', 'page');
	} add_action( 'init', 'set_front_page' );

	/* CHANGE TO PRETTY PERMALINKS
	================================================== */
	function change_permalinks() {
		global $wp_rewrite;
		$wp_rewrite->set_permalink_structure('/%postname%/');
	} add_action( 'after_switch_theme', 'change_permalinks' );

	/* PREVENT SEARCH ENGINES SEEING THE SITE
	================================================== */
	function make_blog_unpublic() {
		update_option('blog_public', '0');
	} add_action( 'after_switch_theme', 'make_blog_unpublic' );

	/* PREPARE CUSTOM DASHBOARD
	================================================== */
	function register_menu() {
		add_dashboard_page( 'Dilate Digital', 'Dilate Digital', 'read', 'custom-dashboard', 'create_dashboard' );
	} add_action('admin_menu', 'register_menu' );

	function redirect_dashboard() {
		if( is_admin() ) {
			$screen = get_current_screen();
			if( $screen->base == 'dashboard' ) {
				wp_redirect( admin_url( 'index.php?page=custom-dashboard' ) );
			}
		}
	} add_action('load-index.php', 'redirect_dashboard' );

	function create_dashboard() {
		include_once( 'custom_dashboard.php'  );
	}

	/* LOAD THEME LANGUAGE
	================================================== */
	/*
	*	You can uncomment the line below to include your own translations
	*	into your child theme, simply create a "language" folder and add your po/mo files
	*/
	
	// load_theme_textdomain('swiftframework', get_stylesheet_directory().'/language');
	
	
	/* REMOVE PAGE BUILDER ASSETS
	================================================== */
	/*
	*	You can uncomment the line below to remove selected assets from the page builder
	*/
	
	// function spb_remove_assets( $pb_assets ) {
	//     unset($pb_assets['parallax']);
	//     return $pb_assets;
	// }
	// add_filter( 'spb_assets_filter', 'spb_remove_assets' );	


	/* ADD/EDIT PAGE BUILDER TEMPLATES
	================================================== */
	function custom_prebuilt_templates($prebuilt_templates) {
			
		/*
		*	You can uncomment the lines below to add custom templates
		*/
		// $prebuilt_templates["custom"] = array(
		// 	'id' => "custom",
		// 	'name' => 'Custom',
		// 	'code' => 'your-code-here'
		// );

		/*
		*	You can uncomment the lines below to remove default templates
		*/
		// unset($prebuilt_templates['home-1']);
		// unset($prebuilt_templates['home-2']);

		// return templates array
	    return $prebuilt_templates;

	}
	//add_filter( 'spb_prebuilt_templates', 'custom_prebuilt_templates' );


	/*    LOAD DILATE MODULES   */
	//##//include_once("swift-framework/swift-framework.php");
	//##//require_once 'modules/endpoints/init.php';

	//init Dilate Endpoint
	//##//new Dilate_Endpoint();

	//CF7 Fallback
	add_filter( 'wpcf7_support_html5_fallback', '__return_true' )
?>