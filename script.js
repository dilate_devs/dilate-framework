/********************************************************************************************/
/* == WAIT-UNTIL-IT-EXIST JQUERY PLUGIN == */
/********************************************************************************************/
(function(e, f) {
    var b = {},
        c
    g = function(a) {
        b[a] && (f.clearInterval(b[a]), b[a] = null)
    };
    e.fn.waitUntilExists = function(a, h, j) {
        var c = this.selector,
            d = e(c),
            k = d.not(function() {
                return e(this).data("waitUntilExists.found")
            });
        "remove" === a ? g(c) : (k.each(a).data("waitUntilExists.found", !0), h && d.length ? g(c) : j || (b[c] = f.setInterval(function() {
            d.waitUntilExists(a, h, !0)
        }, 500)));
        return d
    }
})(jQuery, window);
/********************************************************************************************/
/********************************************************************************************/
/********************************************************************************************/

(function($) {
    /* Dilate Specific functions */
    var dilateJS = {
        init: function() {

        },
        
        iosFixes: function(){
          /* ----------------------------------------------
            ZOOMING IN WHEN FIELD IS IN FOCUS - JQUERY
          ------------------------------------------------- */
          $('head').append("<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />");
          
          /* ----------------------------------------------
            BACKGROUND FIX IN JQUERY VERSION
          ------------------------------------------------- */
          $('.apple-mobile-browser div[data-row-type="image"]').each(function(index,element){
            $(element).css('background',$(element).css('background-image'));
          }); 
          
        }
        };

    var appJS = {
        init: function() {

        }
    }

    $(document).ready(function() {
        //init Dilate Functions
        dilateJS.init();
        dilateJS.iosFixes();

        //init Application level Functions
        appJS.init();
    });
}(window.jQuery || window.$));