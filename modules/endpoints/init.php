<?php
/* */

class Dilate_Endpoint {
	
	/** Hook WordPress
	*	@return void
	*/
	public function __construct(){
		add_filter('query_vars', array($this, 'add_query_vars'), 0);
		add_action('parse_request', array($this, 'sniff_requests'), 0);
		add_action('init', array($this, 'add_endpoint'), 0);
	}
	
	/** Add public query vars
	*	@param array $vars List of current public query vars
	*	@return array $vars 
	*/
	public function add_query_vars($vars){
		$vars[] = '__api';
		$vars[] = 'args';
		return $vars;
	}
	
	/** Add API Endpoint
	*	This is where the magic happens - brush up on your regex skillz
	*	@return void
	*/
	public function add_endpoint(){
		add_rewrite_rule('^dilate/v1/?([0-9]+)?/?','index.php?__api=1&args=$matches[1]','top');
	}

	/**	Sniff Requests
	*	This is where we hijack all API requests
	* 	If $_GET['__api'] is set, we kill WP and serve up pug bomb awesomeness
	*	@return die if API request
	*/
	public function sniff_requests(){
		global $wp;
		if(isset($wp->query_vars['__api'])){
			$this->handle_request();
			exit;
		}
	}
	
	/** Handle Requests
	*	This is where we send off for an intense pug bomb package
	*	@return void 
	*/
  protected function handle_request(){
		global $wp;
		$args = $wp->query_vars['args'];
		//var_dump($wp->query_vars);
		//var_dump($_POST['test']);
		$reqType = $_POST['request']['type'];
    $ret = null;
		if($reqType === "gcf"){
      $keys = $_POST['keys'];
      $postID = $_POST['id'];
    	foreach($keys as $key){
        $ret[$key] = get_post_meta((string)$postID,$key,true); 
      }
      //array_push($ret, array("testing" => get_post_meta('500','property_bedrooms',true))); 
      $this->send_response($ret);
		}
	}
  
	 
	
	/** Response Handler
	*	This sends a JSON response to the browser
	*/
	protected function send_response($msg, $args = ''){
		$response['data'] = $msg;
		//if($args)
		//	$response['args'] = $args;
		header('content-type: application/json; charset=utf-8');
	    echo json_encode($response)."\n";
	    exit;
	}
}